Add rhyme and meter annotation into dataset [55000+ Song Lyrics kaggle](https://www.kaggle.com/mousehead/songlyrics), using the python [pronouncing](https://pronouncing.readthedocs.io).

For example,

lines in lyrics | rhyme annotation |
---|---
Look at her face, it's a wonderful face | 1 1 1 1 1 0 100 1
And it means something special to me | 0 1 1 10 10 1 1
Look at the way that she smiles when she sees me | 1 1 0 1 1 1 1 1 1 1 1
How lucky can one fellow be? | 1 10 1 1 10 1
She's just my kind of girl, she makes me feel fine | 1 1 1 1 1 1 1 1 1 1
Who could ever believe that she could be mine? | 1 1 10 01 1 1 1 1 1
She's just my kind of girl, without her I'm blue | 1 1 1 1 1 01 1 1 1
And if she ever leaves me what could I do, what could I do? | 0 1 1 10 1 1 1 1 1 1 1 1 1 1
And when we go for a walk in the park | 0 1 1 1 1 0 1 0 0 1
And she holds me and squeezes my hand | 0 1 1 1 0 10 1 1
We'll go on walking for hours and talking | 1 1 1 10 1 10 0 10
About all the things that we plan | 01 1 0 1 1 1 1
She's just my kind of girl, she makes me feel fine | 1 1 1 1 1 1 1 1 1 1
Who could ever believe that she could be mine? | 1 1 10 01 1 1 1 1 1
She's just my kind of girl, without her I'm blue | 1 1 1 1 1 01 1 1 1
And if she ever leaves me what could I do, what could I do? | 0 1 1 10 1 1 1 1 1 1 1 1 1 1
